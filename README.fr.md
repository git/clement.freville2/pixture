# Pixture

## Opérations disponibles

- Chargement de plusieurs images à partir du système de fichiers
- Prévisualisation du résultat au centre de l'écran
- Télécharger le résultat actuel sous la forme d'une seule image
- Appliquer une transformation à l'image
- Redimensionner l'image
- Fondu de deux images en faisant glisser l'une d'entre elles sur la zone de travail
- Si l'image est déposée en bas/à droite, elle sera ajoutée à l'image actuelle.
- Créer une animation qui combine toutes les images chargées

## Exécuter un serveur de développement

```bash
npm install
npm run dev
```

## Construire un site statique

```bash
npm run build
```
