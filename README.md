# Pixture

## Available operations

- Load multiple images from the file system
- Preview the result at the center of the screen
- Download the current result as a single image
- Apply a transformation to the image
- Redimension the image
- Fade two images by drag and drop one over the working area
- If dropping at the bottom/at the right, the image will be appended to the current one
- Create an animation that combines all loaded images

## Run a development server

```bash
npm install
npm run dev
```

## Build a static site

```bash
npm run build
```
