export function mergeImages(pixels: Uint8ClampedArray, destination: Uint8ClampedArray, opacity: number = 0.5) {
  for (let i = 0; i < destination.length; i += 1) {
    destination[i] = pixels[i] * (1 - opacity) + destination[i] * opacity;
  }
}
