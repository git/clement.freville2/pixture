export function grayScale(pixels: Uint8ClampedArray): void {
    for (let i = 0; i < pixels.length; i += 4) {
        // https://en.wikipedia.org/wiki/Grayscale#Luma_coding_in_video_systems
        const lightness = (pixels[i] * .299 + pixels[i + 1] * .587 + pixels[i + 2] * .114) | 0;
        pixels[i] = lightness;
        pixels[i + 1] = lightness;
        pixels[i + 2] = lightness;
    }
}

export function blackAndWhite(pixels: Uint8ClampedArray): void {
    for (let i = 0; i < pixels.length; i += 4) {
        const lightness = (pixels[i] * .299 + pixels[i + 1] * .587 + pixels[i + 2] * .114) | 0;
        const value = lightness < 128 ? 0 : 255;
        pixels[i] = value;
        pixels[i + 1] = value;
        pixels[i + 2] = value;
    }
}
