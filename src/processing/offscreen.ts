export function drawOffscreen(data: ImageData): OffscreenCanvas {
  const newCanvas = new OffscreenCanvas(data.width, data.height);
  const newCtx = newCanvas.getContext('2d')!;
  newCtx.putImageData(data, 0, 0);
  return newCanvas;
}
